﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using PolicyBot.Models;
using System.Threading.Tasks;

namespace PolicyBot.Externals
{
    public class MongoConnection
    {
        protected static IMongoClient _client;
        protected static IMongoDatabase _database;
        private const string dbEntryPoint = "test";
        private const string collectionName = "policy";

        public MongoConnection()
        {
            _client = new MongoClient();
            _database = _client.GetDatabase(dbEntryPoint);
        }

        public async Task<Policy> GetPolicyDetailsByKeyAsync(string key)
        {
            Policy policy = null;
            var result = await GetDocumentByKeyAsync(key, collectionName);
            if (!isNullOrEmpty(result))
            {
                DocumentModel dm = BsonSerializer.Deserialize<DocumentModel>(result[0]);
                policy = new Policy();
                policy.policyText = dm.text;
                if (!isNullOrEmpty(dm.related))
                {
                    policy.subpolicies = await GetRelatedPolcies(dm.related);
                }
            }            
            return policy;
        }

        private async Task<Dictionary<string, string>> GetRelatedPolcies(string[] related)
        {
            Dictionary<string, string> relatedPolicies = new Dictionary<string, string>();
            foreach (string item in related)
            {
                var result = await GetDocumentByKeyAsync(item, collectionName);
                if (!isNullOrEmpty(result))
                {
                    DocumentModel dm = BsonSerializer.Deserialize<DocumentModel>(result[0]);
                    relatedPolicies.Add(dm.key, dm.name);
                }
            }
            return relatedPolicies;
        }

        private async Task<List<BsonDocument>> GetDocumentByKeyAsync(string key, string collectionName)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("key", key);
            var collection = _database.GetCollection<BsonDocument>(collectionName);
            var result = await collection.Find(filter).ToListAsync();
            return result;
        }

        private bool isNullOrEmpty(object o)
        {
            return o == null;
        }

        private bool isNullOrEmpty(Array a)
        {
            return ((a == null) || a.Length <= 0);
        }
    }

    public class DocumentModel
    {
        public object _id { get; set; }
        public string key { get; set; }
        public string name { get; set; }
        public string text { get; set; }
        public string[] related;
    }

}